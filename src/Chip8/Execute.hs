module Chip8.Execute (executeInst) where

import Chip8.Data (Chip8 (..), screenWidth, screenHeight)
import Chip8.Instruction (Instruction (..))
import Data.Bits
import Data.Vector ((//), (!))
import Data.Word (Word8, Word16)
import System.Random (randomRIO)
import qualified Data.Vector as V

executeInst :: Instruction -> Chip8 -> IO Chip8

executeInst (CallRCA _) _ = undefined

executeInst ClearDisplay c =
    return $ incrPC $ c { gfx = V.replicate (screenWidth * screenHeight) 0 }

executeInst Return c = return $ c
    { sp = sp c - 1
    , pc = callAddr + 2
    }
  where
    topSp = fromIntegral $ sp c - 1 :: Int
    callAddr = stack c ! topSp

executeInst (Jump addr) c = return $ c { pc = addr }

executeInst (Call addr) c = return $ c
    { stack = stack c // [(fromIntegral (sp c) :: Int, pc c)]
    , sp    = sp c + 1
    , pc    = addr
    }

executeInst (SkipIfEq x val) c = return $ skipIfTrue (vx == val) c
  where
    vx = v c ! (fromIntegral x :: Int)

executeInst (SkipIfNotEq x val) c = return $ skipIfTrue (vx /= val) c
  where
    vx = v c ! (fromIntegral x :: Int)

executeInst (SkipIfEqTo x y) c = return $ skipIfTrue (vx == vy) c
  where
    vx = v c ! (fromIntegral x :: Int)
    vy = v c ! (fromIntegral y :: Int)

executeInst (Set x val) c = return $ incrPC $ c { v = v' }
  where
    v' = v c // [(fromIntegral x :: Int, val)]

executeInst (Increment x val) c =
    incrPC <$> executeInst (Set x incrVal) c
  where
    prevVal = v c ! (fromIntegral x :: Int)
    incrVal = prevVal + val

executeInst (Assign x y) c =
    incrPC <$> executeInst (Set x vy) c
  where
    vy = v c ! (fromIntegral y :: Int)

executeInst (BitOr x y) c =
    incrPC <$> executeInst (Set x val) c
  where
    vx = v c ! (fromIntegral x :: Int)
    vy = v c ! (fromIntegral y :: Int)
    val = vx .|. vy

executeInst (BitAnd x y) c =
    incrPC <$> executeInst (Set x val) c
  where
    vx = v c ! (fromIntegral x :: Int)
    vy = v c ! (fromIntegral y :: Int)
    val = vx .&. vy

executeInst (BitXor x y) c =
    incrPC <$> executeInst (Set x val) c
  where
    vx = v c ! (fromIntegral x :: Int)
    vy = v c ! (fromIntegral y :: Int)
    val = xor vx vy

executeInst (IncrementCarry x y) c = return $ incrPC $ c { v = v' }
  where
    vx = v c ! (fromIntegral x :: Int)
    vy = v c ! (fromIntegral y :: Int)
    vf = if vy > 0xFF - vx then 1 else 0
    v' = v c // [(fromIntegral x :: Int, vx + vy), (0xF, vf)]

executeInst (DecrementCarry x y) c = return $ incrPC $ c { v = v' }
  where
    vx = v c ! (fromIntegral x :: Int)
    vy = v c ! (fromIntegral y :: Int)
    vf = if vx > vy then 1 else 0
    v' = v c // [(fromIntegral x :: Int, vx - vy), (0xF, vf)]

executeInst (ShiftRight x y) c = return $ incrPC $ c { v = v' }
  where
    vy = v c ! (fromIntegral y :: Int)
    vf = vy .&. 0x01
    val = shiftR vy 1
    updates =
        [ (fromIntegral x :: Int, val)
        , (fromIntegral y :: Int, val)
        , (0xF, vf)
        ]
    v' = v c // updates

executeInst (RevDecrement x y) c = return $ incrPC $ c { v = v' }
  where
    vx = v c ! (fromIntegral x :: Int)
    vy = v c ! (fromIntegral y :: Int)
    vf = if vy > vx then 1 else 0
    v' = v c // [(fromIntegral x :: Int, vy - vx), (0xF, vf)]

executeInst (ShiftLeft x y) c = return $ incrPC $ c { v = v' }
  where
    vy = v c ! (fromIntegral y :: Int)
    vf = if vy .&. 0x80 /= 0 then 1 else 0
    val = shiftL vy 1
    updates =
        [ (fromIntegral x :: Int, val)
        , (fromIntegral y :: Int, val)
        , (0xF, vf)
        ]
    v' = v c // updates

executeInst (SkipIfNotEqTo x y) c = return $ skipIfTrue (vx /= vy) c
  where
    vx = v c ! (fromIntegral x :: Int)
    vy = v c ! (fromIntegral y :: Int)

executeInst (SetI addr) c = return $ incrPC $ c { i = addr }

executeInst (JumpAddV0 addr) c = return $ c { pc = addr + v0 }
  where
    v0 = fromIntegral $ v c ! 0 :: Word16

executeInst (RandAnd x val) c = do
    rand <- randomRIO (0, 255)
    let andVal = rand .&. val
    incrPC <$> executeInst (Set x andVal) c

executeInst (DrawSprite _x _y _n) _c = undefined

executeInst (SkipEqKey _x) _c = undefined

executeInst (SkipNotEqKey _x) _c = undefined

executeInst (GetDelayTimer x) c =
    incrPC <$> executeInst (Set x delay) c
  where
    delay = delayTimer c

executeInst (GetKey _x) _c = undefined

executeInst (SetDelayTimer x) c = return $ incrPC $ c
    { delayTimer = v c ! (fromIntegral x :: Int)
    }

executeInst (SetSoundTimer x) c = return $ incrPC $ c
    { soundTimer = v c ! (fromIntegral x :: Int)
    }

executeInst (AddI x) c = return $ incrPC $ c
    { i = i c + (fromIntegral vx :: Word16)
    }
  where
    vx = v c ! (fromIntegral x :: Int)

executeInst _ _ = undefined

skipIfTrue :: Bool -> Chip8 -> Chip8
skipIfTrue cond c
    | cond      = c { pc = pc c + 4 }
    | otherwise = c { pc = pc c + 2 }

incrPC :: Chip8 -> Chip8
incrPC c = c { pc = pc c + 2 }
