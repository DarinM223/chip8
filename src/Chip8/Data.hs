module Chip8.Data
    ( Chip8 (..)
    , createChip8
    , loadFontset
    , loadProgram
    , screenWidth
    , screenHeight
    ) where

import Data.List (foldl')
import Data.Word (Word8, Word16)
import Data.Vector ((//))

import qualified Data.ByteString as B
import qualified Data.Vector as V

screenWidth :: Int
screenWidth = 64

screenHeight :: Int
screenHeight = 32

fontset :: [Word8]
fontset =
    [ 0xF0, 0x90, 0x90, 0x90, 0xF0
    , 0x20, 0x60, 0x20, 0x20, 0x70
    , 0xF0, 0x10, 0xF0, 0x80, 0xF0
    , 0xF0, 0x10, 0xF0, 0x10, 0xF0
    , 0x90, 0x90, 0xF0, 0x10, 0x10
    , 0xF0, 0x80, 0xF0, 0x10, 0xF0
    , 0xF0, 0x80, 0xF0, 0x90, 0xF0
    , 0xF0, 0x10, 0x20, 0x40, 0x40
    , 0xF0, 0x90, 0xF0, 0x90, 0xF0
    , 0xF0, 0x90, 0xF0, 0x10, 0xF0
    , 0xF0, 0x90, 0xF0, 0x90, 0x90
    , 0xE0, 0x90, 0xE0, 0x90, 0xE0
    , 0xF0, 0x80, 0x80, 0x80, 0xF0
    , 0xE0, 0x90, 0x90, 0x90, 0xE0
    , 0xF0, 0x80, 0xF0, 0x80, 0xF0
    , 0xF0, 0x80, 0xF0, 0x80, 0x80
    ]

data Chip8 = Chip8
    { opcode     :: Word16
    , memory     :: V.Vector Word8
    , v          :: V.Vector Word8
    , i          :: Word16
    , pc         :: Word16
    , gfx        :: V.Vector Word8
    , delayTimer :: Word8
    , soundTimer :: Word8
    , stack      :: V.Vector Word16
    , sp         :: Word16
    , keys       :: V.Vector Word8
    , drawFlag   :: Bool
    } deriving (Show)

createChip8 :: Chip8
createChip8 = Chip8
    { opcode     = 0
    , memory     = V.replicate 4096 0
    , v          = V.replicate 16 0
    , i          = 0
    , pc         = 0x200
    , gfx        = V.replicate (screenWidth * screenHeight) 0
    , delayTimer = 0
    , soundTimer = 0
    , stack      = V.replicate 16 0
    , sp         = 0
    , keys       = V.replicate 16 0
    , drawFlag   = False
    }

loadProgram :: B.ByteString -> Chip8 -> Chip8
loadProgram buf c = foldl' setMemory c [0..B.length buf - 1]
  where
    setMemory c i = c { memory = memory' }
      where
        memory' = memory c // [(i + 512, B.index buf i)]

loadFontset :: [Word8] -> Chip8 -> Chip8
loadFontset fontset c = foldl' setFontset c (zip fontset [0..])
  where
    setFontset c (f, i) = c { memory = memory c // [(i, f)] }
