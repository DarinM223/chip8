module Chip8.Instruction
    ( Instruction (..)
    , instFromOp
    ) where

import Data.Bits
import Data.Word (Word8, Word16)

data Instruction
    = CallRCA Word16
    | ClearDisplay
    | Return
    | Jump Word16
    | Call Word16
    | SkipIfEq Word8 Word8
    | SkipIfNotEq Word8 Word8
    | SkipIfEqTo Word8 Word8
    | Set Word8 Word8
    | Increment Word8 Word8
    | Assign Word8 Word8
    | BitOr Word8 Word8
    | BitAnd Word8 Word8
    | BitXor Word8 Word8
    | IncrementCarry Word8 Word8
    | DecrementCarry Word8 Word8
    | ShiftRight Word8 Word8
    | RevDecrement Word8 Word8
    | ShiftLeft Word8 Word8
    | SkipIfNotEqTo Word8 Word8
    | SetI Word16
    | JumpAddV0 Word16
    | RandAnd Word8 Word8
    | DrawSprite Word8 Word8 Word8
    | SkipEqKey Word8
    | SkipNotEqKey Word8
    | GetDelayTimer Word8
    | GetKey Word8
    | SetDelayTimer Word8
    | SetSoundTimer Word8
    | AddI Word8
    | SpriteAddr Word8
    | BCD Word8
    | StoreMem Word8
    | LoadMem Word8
    deriving (Show, Eq)

instFromOp :: Word16 -> Instruction
instFromOp op = instFromOpHeader . splitWord16 $ op

instFromOpHeader :: (Word8, Word8, Word8, Word8) -> Instruction
instFromOpHeader (0, 0, 14, 0) = ClearDisplay
instFromOpHeader (0, 0, 14, 14) = Return
instFromOpHeader (0, a, b, c) = CallRCA $ mergeWord16 a b c
instFromOpHeader (1, a, b, c) = Jump $ mergeWord16 a b c
instFromOpHeader (2, a, b, c) = Call $ mergeWord16 a b c
instFromOpHeader (3, x, a, b) = SkipIfEq x $ mergeWord8 a b
instFromOpHeader (4, x, a, b) = SkipIfNotEq x $ mergeWord8 a b
instFromOpHeader (5, x, y, 0) = SkipIfEqTo x y
instFromOpHeader (6, x, a, b) = Set x $ mergeWord8 a b
instFromOpHeader (7, x, a, b) = Increment x $ mergeWord8 a b
instFromOpHeader (8, x, y, 0) = Assign x y
instFromOpHeader (8, x, y, 1) = BitOr x y
instFromOpHeader (8, x, y, 2) = BitAnd x y
instFromOpHeader (8, x, y, 3) = BitXor x y
instFromOpHeader (8, x, y, 4) = IncrementCarry x y
instFromOpHeader (8, x, y, 5) = DecrementCarry x y
instFromOpHeader (8, x, y, 6) = ShiftRight x y
instFromOpHeader (8, x, y, 7) = RevDecrement x y
instFromOpHeader (8, x, y, 14) = ShiftLeft x y
instFromOpHeader (9, x, y, 0) = SkipIfNotEqTo x y
instFromOpHeader (10, a, b, c) = SetI $ mergeWord16 a b c
instFromOpHeader (11, a, b, c) = JumpAddV0 $ mergeWord16 a b c
instFromOpHeader (12, x, a, b) = RandAnd x $ mergeWord8 a b
instFromOpHeader (13, x, y, n) = DrawSprite x y n
instFromOpHeader (14, x, 9, 14) = SkipEqKey x
instFromOpHeader (14, x, 10, 1) = SkipNotEqKey x
instFromOpHeader (15, x, 0, 7) = GetDelayTimer x
instFromOpHeader (15, x, 0, 10) = GetKey x
instFromOpHeader (15, x, 1, 5) = SetDelayTimer x
instFromOpHeader (15, x, 1, 8) = SetSoundTimer x
instFromOpHeader (15, x, 1, 14) = AddI x
instFromOpHeader (15, x, 2, 9) = SpriteAddr x
instFromOpHeader (15, x, 3, 3) = BCD x
instFromOpHeader (15, x, 5, 5) = StoreMem x
instFromOpHeader (15, x, 6, 5) = LoadMem x
instFromOpHeader (_, _, _, _) = undefined

mergeWord8 :: Word8 -> Word8 -> Word8
mergeWord8 a b = shiftL a 4 .|. b

mergeWord16 :: Word8 -> Word8 -> Word8 -> Word16
mergeWord16 a b c = shift a 8 .|. shift b 4 .|. c'
  where
    c' = fromIntegral c :: Word16
    shift a s = fromIntegral $ shiftL a s :: Word16

splitWord16 :: Word16 -> (Word8, Word8, Word8, Word8)
splitWord16 i = (mask i1, mask i2, mask i3, mask i4)
  where
    mask i = fromIntegral $ i .&. 0xFF :: Word8
    i1 = i
    i2 = shiftR i1 4
    i3 = shiftR i2 4
    i4 = shiftR i3 4
