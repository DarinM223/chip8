module Chip8 (emulateCycle) where

import Chip8.Data (Chip8 (..))
import Chip8.Instruction (Instruction (..), instFromOp)
import Chip8.Execute (executeInst)
import Data.Bits
import Data.Word (Word8, Word16)
import Data.Vector ((!))

emulateCycle :: Chip8 -> IO Chip8
emulateCycle c = updateTimers =<< executeInst inst c
  where
    inst = instFromOp . fetchOpcode $ c

updateTimers :: Chip8 -> IO Chip8
updateTimers = updateSoundTimer . updateDelayTimer
  where
    updateDelayTimer c
        | delayTimer c > 0 = c { delayTimer = delayTimer c - 1 }
        | otherwise        = c
    updateSoundTimer c
        | soundTimer c == 1 = putStrLn "BEEP" >> return c { soundTimer = 0 }
        | soundTimer c > 0  = return c { soundTimer = soundTimer c - 1 }
        | otherwise         = return c

fetchOpcode :: Chip8 -> Word16
fetchOpcode c = mergeWord16 a b
  where
    i = fromIntegral $ pc c :: Int
    a = memory c ! i
    b = memory c ! i + 1

mergeWord16 :: Word8 -> Word8 -> Word16
mergeWord16 a b = shift a 8 .|. b'
  where
    b' = fromIntegral b :: Word16
    shift a b = fromIntegral $ shiftL a b :: Word16
